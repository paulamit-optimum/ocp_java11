package studyguide.assessment;

interface Service1{
    int getServiceID();
} // end of Service1

/*
abstract class OptimumService1 implements Service1{
    protected int getServiceID(){
        return 4;
    } // end of getServiceID
} // end of OptimumService1

public class TestCode02 implements  Service1{

    //@Override
    public int getServiceID(int ID) {
        return 2;
    } // end of getServiceID

    public static void main(String[] args) {
        var OptimumService1Ref = new OptimumService1();
        System.out.println(OptimumService1Ref.getServiceID());
    } // end of main
} // end of TestCode02
*/