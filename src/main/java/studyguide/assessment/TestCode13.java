package studyguide.assessment;

public class TestCode13 {
    public TestCode13(String refString){
        name = refString;
    }
    public static void main(String[] args) {
        TestCode13 refTestCode13One = new TestCode13("a1");
        TestCode13 refTestCode13Two = new TestCode13("a2");
        refTestCode13One = refTestCode13Two;
        refTestCode13Two = null; // eligible for garbage collection
        refTestCode13One = null;
    }
    private String name;
}
