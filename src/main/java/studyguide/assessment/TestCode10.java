package studyguide.assessment;

public class TestCode10 {
    public static void main(String[] args) {
        var x = 5;
        var j = 0;

        OUTER:
        for (var i = 0; i < 3; )
            INNER:
            do {
                i++;
                x++;
                if (x > 10)
                    break INNER;
                System.out.println("X = "+x);  // x = 6
                System.out.println("Y = "+j);  // y = 0
                x += 4;
                System.out.println("X = "+x);  // x = 10
                System.out.println("Y = "+j);  // y = 0
                j++;
                System.out.println("Y = "+j);  // y = 1
            } // end of do
            while (j <= 2);
            System.out.println(x); // x = 12
        } // end of main
    } // end of class
