package studyguide.assessment;

public class TestCode15 {

    // True ot False

    // Checked exceptions are intended to be thrown by the JVM (and not the programmer) ==> False
    // Checked exceptions are required to be handled or declared ==> True
    // Errors are intended to be thrown by the JVM (and not the programmer) ==> True
    // Errors are required to be caught or declared ==> False
    // Runtime exceptions are intended to be thrown by the JVM (and not the programmer) ==> False
    // Runtime exceptions are required to be handled or declared ==> False

}
