package studyguide.assessment;

public class TestCode01 {

    public static void addToInt(int number1, int number2){
        number1 = number1 + number2;
    } // end of addToInt

    public static void main(String[] args) {

        var aRef = 15;
        var bRef = 10;

        TestCode01.addToInt(aRef, bRef);
        System.out.println(aRef);
    } // end of main

} // end of TestCode01

