package studyguide.assessment;

import java.util.function.Predicate;

public class TestCode07 {

    public static void main(String[] args) {
        System.out.println(testCase((Integer i) -> {return i==5;}));
    }

    private static boolean testCase(Predicate<Integer> refPredicate){
        return refPredicate.test(5);
    }

}


// System.out.println(test(i -> i==5)); // true
// System.out.println(testCase((i) -> i==5));
// System.out.println(testCase((Integer i) -> {return i==5;}));
// System.out.println(testCase((i) -> {return i==5;}));