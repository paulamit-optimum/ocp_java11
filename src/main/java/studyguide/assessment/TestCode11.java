package studyguide.assessment;

import java.io.FileNotFoundException;

class Service3{
    public Service3(){
        System.out.print("service3");
    }
    public Service3(int age){
        System.out.print("service3Age");
    }
    protected boolean hasAccess(){
        return  false;
    }
} // end of Service3

public class TestCode11 extends Service3{
    public  TestCode11(int age) {
        System.out.print("TestCode11");
    }
    public boolean hasAccess(){
        return  true;
    }

    public static void main(String... args) throws FileNotFoundException {
        Service3 refService3 = new TestCode11(5);
        System.out.println(","+refService3.hasAccess());
    }
}
