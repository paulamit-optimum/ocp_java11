package studyguide.assessment.pack2;

import studyguide.assessment.pack1.ParentClass;

public class ChildClass extends ParentClass {
    public static void main(String[] args) {
        ChildClass refChildClass1 = new ChildClass();
        refChildClass1.data1();
    //    refChildClass1.data2();

        ParentClass refParentClass2 = new ChildClass();
    //    refParentClass2.data1();
    //    refParentClass2.data2();

    }

}
