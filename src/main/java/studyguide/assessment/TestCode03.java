package studyguide.assessment;

public class TestCode03 {

    public static void main(String[] args) {
        int var1 = 9, var2 = 2 + 2 * 3;
        float var3 = var2>10 ? 1:3;
        double var4 = (var1+var2) - 1.0f;
        int var5 = --var1 <= 8 ? 2 : 3;
        System.out.println(var3+ "-"+var4+"-"+var5);

    } // main
} // end of TestCode03

// Output: 3.0-16.0-2