package studyguide.assessment;

public class TestCode09 {
    public static void main(String[] args) {

        // Expressions that compile without error

        // int var1 = 3+6.0;
        double var2 = 5_6L;
        // boolean var3 = 1>2 ? !true;
        short var4 = (short) Integer.MAX_VALUE;
        // long var5 = 8.0L;
        // var var6 = 2_.0;

    }
}
