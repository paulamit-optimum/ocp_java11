package studyguide.assessment;

public class TestCode04 {

    public static void main(String[] args) {

        var var1 = "Java";
        var var2 = "Java";
        var var3 = "Ja".concat("va");
       // System.out.println(var3);
        var var4 = var3.intern();
       // System.out.println(var4);
        var var5 = new StringBuilder();
        var5.append("Ja").append("va");
       // System.out.println(var5);
        System.out.println(var1==var2); // true
        System.out.println(var1.equals(var2)); // true
        System.out.println(var1==var3); // false
        System.out.println(var1==var4); // true
        System.out.println(var5.toString()==var1); // false
        System.out.println(var5.toString().equals(var1)); // true

    } // end of main
} // end if TestCode04
