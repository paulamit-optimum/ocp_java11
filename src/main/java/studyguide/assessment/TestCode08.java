package studyguide.assessment;

import java.util.ArrayList;

interface BodyMind{ }

class Yoga implements BodyMind { }

abstract class Pilates extends  Yoga { }

public class TestCode08 {

   public static void main(String[] args) {
        var refPilates = new ArrayList<Pilates>();
        for(Yoga refYoga : refPilates){
            Object pilate = refYoga;
            // instead of Object Yoga or BodyMind will not provide compilation error
        }
    }
}
