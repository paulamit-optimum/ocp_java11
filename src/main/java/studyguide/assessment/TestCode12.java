package studyguide.assessment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestCode12 {
    public static void main(String[] args) {
        int[] array = {6,9,8};
        List<Integer> refList = new ArrayList<>();
        refList.add(array[0]);
        System.out.println(refList); // [6]
        refList.add(array[2]);
        System.out.println(refList); // [6 8]
        refList.set(1,array[1]);
        System.out.println(refList); // [6 9]
        refList.remove(0);
        System.out.println(refList); // [9]
        System.out.println("C" + Arrays.compare(array, new int[] {6,9,8})); // 0 means true ==> same
        System.out.println("M" + Arrays.mismatch(array,new int[] {6,9,8})); // 1 means false ==> not mismatch
    }
}
