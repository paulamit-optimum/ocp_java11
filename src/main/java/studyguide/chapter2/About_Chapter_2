Chapter 2
=========
    Java Building Blocks
    --------------------

    -> Working with Java Primitive Data Types and String APIs
            -> Declare and initialize variables (casting)
            -> Identify the scope of the variables
            -> Use local variable type inference

    -> Describing and Using Objects and Classes
            -> Declare and instantiate Java objects, and explain object lifecycle
               (object creation -> de-referencing by reassignment, and garbage collection)
            -> Read or write to object fields



    Exam Essentials
    ---------------

    -> Be able to recognize a constructor :
            - A constructor has the same same as the class.
            - It looks like a method without a return type.

    -> Be able to identify legal and illegal declarations and initialization    :
            - Local variables require an explicit initialization;
              global variables use the default value for that type
            - Identifiers may contain letters, numbers, $ or _ (can't be begin with numbers)
            - Numeric literals may contains underscores between two digits

    -> Be able to use var correctly :
            - var is used for a local variable inside a constructor / method / block
            - can't be used inside constructor parameter / method parameter / instance or class variables
            - a var is initialized on the same line where it is declared
            - a var can't be initialized with a null value without a type
            - a var can't be used in multiple variables declarations
            - var is not a reserved word in Java and can be used as variable name

    -> Be able to determine where variables fo into and our of scope
            - All variables go into scope when they are declared.
            - Local variables go out of the scope when the block they are declared in ends
            - Instance variables go out of the scope when the object is eligible for garbage collection
            - Class variables remains in scope as long as the program is running

    -> Know how to identify when an object is eligible for garbage collection
            -  pointing to null
