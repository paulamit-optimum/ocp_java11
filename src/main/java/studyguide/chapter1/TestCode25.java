package studyguide.chapter1;

public class TestCode25 {

}

/*

package abc1;
public class Manager{
    boolean  id;
}

package abc1.abc2;
public class Manager{
    boolean name = true;
}


package xyz;
// insert code

public class Filter{
    Manager refManager;
}

*/

// Which of the following snippets can independently be inserted at Line 21

// A. import abc1.*;                [ True ]

// B. import abc1.Manager;          [ True ]
//    import abc1.abc2.*;

// C. import abc1.*;                [ True ]
//    import abc1.abc2.Manager;

// D. import abc1.*;                [ True ]
//    import abc1.abc2.*;

