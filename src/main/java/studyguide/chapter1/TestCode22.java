package studyguide.chapter1;

public class TestCode22 {
    public static void main(String[] args) {
        TestCode22 refTestCode22 = new TestCode22();
    } // end of main
} // end of TestCode22

// Which of the following are true

// A. TestCode22 is a class [ True ]
// B. main is a class
// C. TestCode22 is a reference to an object
// D. main is a reference to an object
// E. main() method doesn't run because the parameter name is incorrect
// F. refTestCode22 is a reference to an object [ True ]
