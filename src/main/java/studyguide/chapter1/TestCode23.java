package studyguide.chapter1;

public class TestCode23 {
    public void main(String[] args) {
        for(int i=1; i<=args.length; i++){
            System.out.println(args[i]);
        } // end of for
    } // end of main
} // end of TestCode23

// Command Line Argument : Hello1 Hello2

// Output Options:

// A. Hello1 Hello2
// B. Hello1
// C. Hello2
// D. Exception at runtime [ True ]