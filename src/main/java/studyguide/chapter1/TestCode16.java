package studyguide.chapter1;

import java.sql.Date;
import java.util.Calendar;
import java.util.Random;

public class TestCode16 {
    public static void main(String... args) {
        Random refRandom = new Random();
        System.out.println("Date using Random : "+refRandom.nextInt());

        Date refDate1 = new Date(System.currentTimeMillis()); // sql.Date
        System.out.println("Date using currentTimeMillis : "+refDate1);

        Date refDate2 = new Date(Calendar.getInstance().getTimeInMillis());
        System.out.println("Date using Calendar.getInstance() : "+refDate2);

        java.util.Date refDate3 = new java.util.Date();
        System.out.println("Date using java.sql.Time : "+new java.sql.Time(refDate3.getTime()));
    }
}
