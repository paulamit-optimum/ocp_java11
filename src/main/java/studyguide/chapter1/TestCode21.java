package studyguide.chapter1;

public class TestCode21 {
    public static void main(String[] args) {

        // Which options are valid on the javac command without considering module options
        // Which options are valid on the java command without considering module options
        // A. -c
        // B. -C
        // C. -cp   [ True for javac and java command ]
        // D. -CP   [ true for javac command ]
        // E. -d
        // F. -f
        // G. -p
    }
}

// Command to Create and Run a jar file

// save file as OptimumJarFile.java
// > javac OptimumJarFile.java

// > jar -cvf OptimumJarFile.jar OptimumJarFile.class

// > jar -cvf OptimumJarFile.jar OptimumJarFile.class

// > java -cp ./packageName/OptimumCloneJarFile.jar OptimumJarFile

// reference : https://www.webucator.com/article/how-to-create-a-jar-file-in-java/



