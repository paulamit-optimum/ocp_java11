package studyguide.chapter1;

public class TestCode24 {
    public static void main(String[] args) {
        System.out.println("hello");
    }
}

// Which of the following are true if this command completes successfully?

// A. TestCode24 .class file created
// B. TestCode24 can reference classes in the package java.lang [ True ]
// C. TestCode24 can reference classes in the package java.util [ True ]
// D. None of the above
