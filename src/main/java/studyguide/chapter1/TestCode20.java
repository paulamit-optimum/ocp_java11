package studyguide.chapter1;

public class TestCode20 {

    public static void main(String[] args) {

        // Which of the following statements are true about Java?

        // A. Bug free code is guaranteed
        // B. Deprecated features are never removed
        // C. Multithreaded code is allowed [ True ]
        // D. Sideways compatibility is a design goal
        // E. Security is a design goal [ True ]

        ABC refAbc = new ABC() {
            @Override
            public void info1() {
                System.out.println("Info 1..");
            }

            @Override
            public int getInfo2() {
                return 10;
            }
        };
        refAbc.info1();
        System.out.println(refAbc.getInfo2());
        System.out.println(refAbc.getBusinessLogic("hello"));
    }
}


// Java supports ==> backward compatibility

interface ABC{
    // old methods
    void info1();
    int getInfo2();

    // new added methods
    default Object getBusinessLogic(String str){

        if(str==null){
            return null;
        }
        str = "found data";

        return str;
    } // end of getBusinessLogic

} // end of ABC