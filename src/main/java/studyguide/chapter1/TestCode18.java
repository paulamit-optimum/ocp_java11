package studyguide.chapter1;

import java.util.ArrayList;

public class TestCode18 {
    public static void main(String[] args) {
        // Which of the following are true

        // A. javac compiles a .class file into a .java file
        // B. javac compiles a .java file into a .bytecode file
        // C. javac compiles a .java file into a .class file [ True ]
        // D. java accepts the name of the class as a parameter [ True ]
        // E. java accepts the filename of the .bytecode file as a parameter
        // F. java accepts the filename of the .class file as a parameter

        Demo1 refDemo1 = new Demo1();
        Demo2 refDemo2 = new Demo2();
        ArrayList refArrayList = new ArrayList();
        refArrayList.add(refDemo1);
        refDemo2.getData(refArrayList);

        // using class.ForName
        try {
            Class refClass1 = Class.forName("studyguide.chapter1.Demo1");
            System.out.println(refClass1.getMethods().length);

            Demo1 refDemo11 = (Demo1)Class.forName("studyguide.chapter1.Demo1").newInstance();
            System.out.println("using class.forName()"+refDemo11);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}

// D : explanation :

// Using reflection it is possible. Here for a given className (passed as a string) .
// This class will be searched in memory ( it should be already loaded).

class Demo1{
    @Override
    public String toString() {
        return "hello";
    }
    public void method2(){
        System.out.println("Hello from method2..");
    }
    public void method3(){
        System.out.println("Hello from method3..");
    }
    static {
        System.out.println("inside Static block");
    }

    {
        System.out.println("inside Instance block");
    }
    Demo1(){
        System.out.println("Default constructor..");
    }
}

class Demo2{
    public void getData(ArrayList refList){
        System.out.println(refList);
    }
}